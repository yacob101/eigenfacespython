# facerec.py
import cv2, sys, numpy, os
fn_haar = 'haarcascade_frontalface_default.xml'
facesFolder = 'facesFolder'
size = 1
# Part 1: Create EigenFaceRecognizer
print('Eigenfaces starting...')

# Create a list of images and a list of corresponding names
(images, lables, names, id) = ([], [], {}, 0)

# Get the folders containing the training data
for (subdirs, dirs, files) in os.walk(facesFolder):

    # Loop through each folder named after the subject in the photos
    for subdir in dirs:
        names[id] = subdir
        subjectpath = os.path.join(facesFolder, subdir)

        # Loop through each photo in the folder
        for filename in os.listdir(subjectpath):

            # Skip non-eigenfaceImage formates
            f_name, f_extension = os.path.splitext(filename)
            if(f_extension.lower() not in
                    ['.png','.jpg','.jpeg','.gif','.pgm']):
                print("Skipping "+filename+", wrong file type")
                continue
            path = subjectpath + '/' + filename
            lable = id

            # Add to training data
            images.append(cv2.imread(path, 0))
            lables.append(int(lable))
        id += 1
(im_width, im_height) = (500, 500)

# Create a Numpy array from the two lists above
(images, lables) = [numpy.array(lis) for lis in [images, lables]]

# OpenCV trains a model from the images
# NOTE FOR OpenCV2: remove '.face'
model = cv2.face.EigenFaceRecognizer_create()
model.train(images, lables)

# Part 2: Use Eigenfaces
haar_cascade = cv2.CascadeClassifier(fn_haar)
eigenfaceImage =  cv2.imread('frontphoto.jpg')

    # Loop until the camera is working
   

    # Flip the eigenfaceImage (optional)
    

    # Convert to grayscale
gray = cv2.cvtColor(eigenfaceImage, cv2.COLOR_BGR2GRAY)

    # Resize to speed up detection (optinal, change size above)
sized = cv2.resize(gray, (int(gray.shape[1] / size), int(gray.shape[0] / size)))

    # Detect faces and loop through each one
faces = haar_cascade.detectMultiScale(sized)
for i in range(len(faces)):
        face_i = faces[i]

        # Coordinates of face after scaling back by `size`
        (x, y, w, h) = [v * size for v in face_i]
        face = gray[y:y + h, x:x + w]
        face_resize = cv2.resize(face, (im_width, im_height))

        # Try to recognize the face
        prediction = model.predict(face_resize)
        cv2.rectangle(eigenfaceImage, (x, y), (x + w, y + h), (0, 255, 0), 3)

        # [1]
        # Write the name of recognized face
        cv2.putText(eigenfaceImage,
           '%s - %.0f' % (names[prediction[0]],prediction[1]),
           (x-10, y-10), cv2.FONT_HERSHEY_PLAIN,1,(255, 255, 0))

    # Show the eigenfaceImage and check for ESC being pressed
cv2.imshow('OpenCV', eigenfaceImage)
cv2.waitKey(0)
cv2.destroyAllWindows()
